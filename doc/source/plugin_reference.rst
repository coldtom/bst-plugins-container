This package provides the following `BuildStream plugins
<https://buildstream.gitlab.io/buildstream/core_plugins.html#external-plugins>`_:

Element Plugins
===============

.. toctree::
   :maxdepth: 1

   elements/docker_image

Source Plugins
==============

.. toctree::
   :maxdepth: 1

   sources/docker
